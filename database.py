from sqlalchemy import create_engine
from sqlalchemy import Table, Column, Integer, String, MetaData
# Global Variables
SQLITE = 'sqlite'

# Table Names
USERS = 'users'


class MyDatabase:
    # http://docs.sqlalchemy.org/en/latest/core/engines.html
    DB_ENGINE = {
        SQLITE: 'sqlite:///{DB}'
    }

    # Main DB Connection Ref Obj
    db_engine = None

    def __init__(self, dbtype, username='', password='', dbname=''):
        dbtype = dbtype.lower()
        if dbtype in self.DB_ENGINE.keys():
            engine_url = self.DB_ENGINE[dbtype].format(DB=dbname)
            self.db_engine = create_engine(engine_url)
            print(self.db_engine)
        else:
            print("DBType is not found in DB_ENGINE")

    def create_db_tables(self):
        metadata = MetaData()
        users = Table(USERS, metadata,
                      Column('id', Integer, primary_key=True),
                      Column('email', String, nullable=False),
                      Column('password', String, nullable=False)
        )
        try:
            metadata.create_all(self.db_engine)
            print("Tables created")
        except Exception as e:
            print("Error occurred during Table creation!")
            print(e)

    # Insert, Update, Delete
    def execute_query(self, query=''):
        if query != '':
            print(query)

        with self.db_engine.connect() as connection:
            try:
                return connection.execute(query)
            except Exception as e:
                print(e)

    # Select
    def execute_select_query(self, query=''):
        if query != '':
            print(query)

        connection = self.db_engine.connect()
        try:
            rs = connection.execute(query)
            return rs.fetchone()[0]  # returns the user id
        except Exception as e:
            print(e)

    def print_all_data(self, table='', query=''):
        query = query if query != '' else "SELECT * FROM '{}';".format(table)
        print(query)
        
        with self.db_engine.connect() as connection:
            try:
                result = connection.execute(query)
            except Exception as e:
                print(e)
            else:
                for row in result:
                    print(row)  # print(row[0], row[1], row[2])
                result.close()
                print("\n")

    def insert(self):
        # Insert Data
        query = "INSERT INTO {}(id, email, password) " \
                "VALUES (3, 'howiek03@gmail.com','123');".format(USERS)
        self.execute_query(query)
        self.print_all_data(USERS)

    def get_user_by_email_password(self, email, pw):
        # Sample Query
        query = "SELECT * FROM {TBL_USR} WHERE " \
                "email = '{email}' and password = '{pw}';".format(TBL_USR=USERS, email=email, pw=pw)
        result = self.execute_select_query(query)
        print(result)
        return result
