from fastapi import FastAPI, Form
from starlette.middleware.cors import CORSMiddleware
from pydantic import BaseModel

from database import MyDatabase, SQLITE, USERS


class Item(BaseModel):
    email: str
    password: str


app = FastAPI()

origins = [
    "http://localhost",
    "http://localhost:4200",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/")
async def read_root():
    return {"Hello": "World"}


@app.get("/items/{item_id}")
async def read_item(item_id: int, q: str = None):
    return {"item_id": item_id, "q": q}


@app.post("/login")
async def create_item(request: Item):
    dbms = MyDatabase(SQLITE, dbname='mydb.sqlite')
    user_id = dbms.get_user_by_email_password(request.email, request.password)
    return {"user_id": user_id}
